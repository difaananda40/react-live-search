import React, { useState, useEffect, useRef } from 'react';
import {
  Container,
  Row,
  Col,
  Form,
  Card,
  InputGroup,
  ListGroup,
  Spinner
} from 'react-bootstrap';
import axios from 'axios';
import _ from 'lodash';

const App = () => {

  const [ states, setStates ] = useState({
    loading: false,
    query: '',
    data: [],
  })

  const dbnc = useRef(_.debounce(func => func(), 500)).current;

  const handleSearch = event => {
    const { value } = event.target;
    setStates(prevState => ({
      ...prevState,
      query: value
    }))
  }

  useEffect(() => {
    if(states.query) {
      let unmounted = false;
      let source = axios.CancelToken.source();
      const apiCall = () => {
        setStates(prevState => ({
          ...prevState,
          loading: true
        }))

        axios.get('http://localhost:1337/products', {
          cancelToken: source.token,
          params: {
            name_contains: states.query
          }
        }).then(res => {
          if(!unmounted) {
            setStates(prevState => ({
              ...prevState,
              loading: false,
              data: res.data
            }))
          }
        })
      }
      dbnc(() => apiCall())
      return (() => {
        unmounted = true;
        source.cancel();
      })
    }
  }, [states.query, dbnc])

  return (
    <Container>
      <Row>
        <Col className="d-flex align-items-center justify-content-center my-5">
          <Card style={{ width: '50%' }}>
            <Card.Body>
              <Row>
                <Col>
                  <InputGroup>
                    <Form.Control
                      type="text"
                      placeholder="Search here ..."
                      onChange={handleSearch}
                      value={states.query}
                    />
                    <InputGroup.Append>
                      <InputGroup.Text>
                        { states.loading ? 'Searching' : 'Search' }
                      </InputGroup.Text>
                    </InputGroup.Append>
                  </InputGroup>
                </Col>
              </Row>
              {
                states.loading
                ? (
                  <Row className="mt-4">
                    <Col className="d-flex justify-content-center">
                      <Spinner animation="grow" />
                    </Col>
                  </Row>
                )
                : 
                  states.data.length >= 1
                  ? (
                    <Row className="mt-4">
                      <Col>
                        <ListGroup>
                          {
                            states.data.map(dt => (
                              <ListGroup.Item key={dt.id}>{dt.name}</ListGroup.Item>
                            ))
                          }
                        </ListGroup>
                      </Col>
                    </Row>
                  )
                  : (
                    <Row className="mt-4">
                      <Col>No data found.</Col>
                    </Row>
                  )
              }
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}

export default App;